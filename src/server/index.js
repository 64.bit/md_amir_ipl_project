const csvtojson = require('csvtojson');
const fs = require('fs');
const { matchesPlayedPerYear, matchesWonPerYear, runScoredByEachTeam, findtopEconomicBowlers } = require('./ipl');

const convertToJson = async () => {

    //converting data in match.csv to json
    const matches = await csvtojson().fromFile(`${__dirname}/../data/matches.csv`);

    //converting data in deliveries.csv to json
    const deliveries = await csvtojson().fromFile(`${__dirname}/../data/deliveries.csv`);

    
    const numberOfMatchesPlayedPerYear = matchesPlayedPerYear(matches);

    fs.writeFileSync(`${__dirname}/../public/output/numberOfMatchesPlayerPerYear.json`, JSON.stringify(numberOfMatchesPlayedPerYear));


    const teamList = matchesWonPerYear(matches);

    fs.writeFileSync(`${__dirname}/../public/output/numberOfMatchesWonPerTeamPerYear.json`, JSON.stringify(teamList));


    const perTeamRuns = runScoredByEachTeam(deliveries, matches);

    fs.writeFileSync(`${__dirname}/../public/output/extraRunsPerTeam.json`, JSON.stringify(perTeamRuns));


    const topEconomicBowlers = findtopEconomicBowlers(deliveries, matches);
    
    fs.writeFileSync(`${__dirname}/../public/output/topTenEconomicBowlers.json`, JSON.stringify(topEconomicBowlers));
}
convertToJson();
