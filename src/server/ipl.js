const matchesPlayedPerYear = (matches) => {

    //hashmap for storing number of matches playes
    let hash = {};
    for (let index = 0; index < matches.length; index++) {
        //checking is there a key exists or not
        if (!hash.hasOwnProperty(matches[index].season))
             hash[matches[index].season] = 0; //intializing the season as 0
        
        //incrementing the value of current season by one
        hash[matches[index].season]++;
    }
    return hash;
}
const matchesWonPerYear = (matches) => {

    let teamList = {};

    //initalizing the hashmap where key is winner team and value is {}
    for (let index = 0; index < matches.length; index++) {
        teamList[matches[index].winner] = {};
    }

    for (let index = 0; index < matches.length; index++) {

        // checking if current winner team has a property innitialized whose key is equal to current match's season or not
        if (!teamList[matches[index].winner].hasOwnProperty(matches[index].season)) 
            teamList[matches[index].winner][matches[index].season] = 0;  //if not then we will initialize it

        // incrementing current winner's season
        teamList[matches[index].winner][matches[index].season]++;
    }
    return teamList;
}

//binary search function to get first index of the year 
const lowerBound = (id, deliveriesOf2016) => {

    let low = 0, high = deliveriesOf2016.length - 1;

    while (low < high) {

        let mid = Math.floor((low + high) / 2);

        if (deliveriesOf2016[mid].match_id === id) 
            high = mid;

        else if (deliveriesOf2016[mid].match_id > id) 
            high = mid - 1;

        else 
            low = mid + 1;
    }
    return high;
}
// binay search function to get last index+1 of the year
const upperBound = (id, deliveriesOf2016) => {

    let low = 0, high = deliveriesOf2016.length - 1;

    while (low < high) {

        let mid = Math.floor((low + high) / 2);

        if (id >= deliveriesOf2016[mid].match_id) 
            low = mid + 1;

        else 
            high = mid;
    }

    
    if ((low < deliveriesOf2016.length) && (deliveriesOf2016[low].match_id <= id)) {
        low++;
    }
    return low;
}

//helper function to get alldeliveries of a particular year
const getDeliveriesOfYear = (deliveries, matches, year) => {

    //for storing all matches of a particular year
    const matchesOfYear = [];

    for (let index = 0; index < matches.length; index++){

        //if current match season is equal to required match year
        if (matches[index].season === year) 
        matchesOfYear.push(matches[index]);

    }

    //for storing all deliveries of a prticular year
    let deliveriesOfYear = [];

    for (let index = 0; index < deliveries.length; index++) {

        //if current deleveries match_id is exists in all matches of a prticular year this delivery is related to that particular year
        if (matchesOfYear.find(match => match.id === deliveries[index].match_id)) {
            deliveriesOfYear.push(deliveries[index]);
        }
    }
    return { matchesOfYear, deliveriesOfYear };
}
const runScoredByEachTeam = (deliveries, matches) => {

    let extraRuns = {}

    const requiredDeliveries = getDeliveriesOfYear(deliveries, matches, '2016');//getting information related to a year 2016

    const matchesOf2016 = requiredDeliveries.matchesOfYear;//extracting matches of 2016

    const deliveriesOf2016 = requiredDeliveries.deliveriesOfYear;//extracting all deleveries of the year 2016

    for (let index = 0; index < matchesOf2016.length; index++) {

        const low = lowerBound(matchesOf2016[index].id, deliveriesOf2016);//for finding first index of the the current matchid in deleveries2016 array

        const high = upperBound(matchesOf2016[index].id, deliveriesOf2016);//for finding last index+1 of the the current matchid in deleveries2016 array

        for (let i = low; i < high; i++) {

            // if extraRuns does not contain a property named as bowling team then initialize
            if (!extraRuns.hasOwnProperty(deliveriesOf2016[i].bowling_team)) 
                extraRuns[deliveriesOf2016[i].bowling_team] = 0;
            
            // increase the runs bof current bowling_team by extra runs
            extraRuns[deliveriesOf2016[i].bowling_team] += parseInt(deliveriesOf2016[i].extra_runs);
        }
    }
    return extraRuns;
}
const findtopEconomicBowlers = (deliveries, matches) => {

    //for storing allbowlers of year 2015
    let allBowlers = new Map(); //map will contain key=bowler name, value=[runs,bowls]

    const requiredDeliveries = getDeliveriesOfYear(deliveries, matches, '2015');

    const deliveriesOf2015 = requiredDeliveries.deliveriesOfYear; //getting all deleveries of 2015

    for (let index = 0; index < deliveriesOf2015.length; index++) {
        //if current bowler doesn't exist in map ten initialize
        if (!allBowlers.has(deliveriesOf2015[index].bowler))
            allBowlers.set(deliveriesOf2015[index].bowler, [0, 0]);
        
        // getting the current values of the bowler
        const value = allBowlers.get(deliveriesOf2015[index].bowler);

        allBowlers.set(deliveriesOf2015[index].bowler, [value[0] + parseInt(deliveriesOf2015[index].total_runs), value[1] + 1]);//runs will be incremented by total runs in this bowl & bowls will be incremented by one
    }
    // for storing the bowlers sorted by average runs per ball
    let sortedByAvg = [];

    //storing the economy per ball and name of the bowler in sortedByAvg array
    allBowlers.forEach((value, key) => sortedByAvg.push({ runsPerBall: value[0] / value[1], name: key }));

    //sorting by economy
    sortedByAvg.sort((first, second) => first.runsPerBall - second.runsPerBall)

    //for storing top 10 exonomic bowlers
    let topTenEconomicBowlers = [];

    //loop through bowler 1 to bowler 10 to get top 10 economic bowlers
    for (let index = 0; index < 10; index++) 
        topTenEconomicBowlers.push(sortedByAvg[index].name);

    return topTenEconomicBowlers;
}
module.exports = { matchesPlayedPerYear, matchesWonPerYear, runScoredByEachTeam, findtopEconomicBowlers };